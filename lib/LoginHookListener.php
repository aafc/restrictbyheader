<?php

namespace OCA\RestrictByHeader;

use OCP\IConfig;
use OCP\IRequest;
use OC\Group\Manager;
use OCP\IURLGenerator;

class LoginHookListener {
	/** @var IConfig */
	private $config;
	/** @var IRequest */
	private $request;
	/** @var Manager  */
	private $groupManager;
	/** @var IURLGenerator */
	private $urlGenerator;
	/** @var bool */
	private $isLoginPage;

	public function __construct(IConfig $config,
								IRequest $request,
								Manager $groupManager,
								IURLGenerator $urlGenerator,
								$isLoginPage) {
		$this->config = $config;
		$this->request = $request;
		$this->groupManager = $groupManager;
		$this->urlGenerator = $urlGenerator;
		$this->isLoginPage = $isLoginPage;
	}

	/**
	 * Checks if user is in any of the restricted groups.
	 * @return bool
	 */
	private function inRestrictedGroups($params) {
		$restrictedGroups = $this->config->getAppValue('restrictbyheader', 'blacklisted.groups', '');
		if($restrictedGroups === '') { return false; }
		$restrictedGroups = explode(',', $restrictedGroups);

		$uid = $params['uid'];
		foreach($restrictedGroups as $groupId) {
			if ($this->groupManager->isInGroup($uid, $groupId)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the HTTP request indicates external user is logging in. 
	 * @return bool
	 */
	private function isHeaderPresent() {
		if ($this->request->getHeader("LLI-SOURCE") == "WORLD"){
			return true;
		}
		return false;
	}

	/**
	 * Prelogin check. Two conditions must be present to reject user entry:
	 * 	1. User has HTTP Header "LLI-SOURCE: WORLD" (set in LLI WAF)
	 * 	2. User is with any of the restricted groups (specified in Admin Settings)
	 */
	public function handleLoginRequest($user) {
		// Web UI
		if ($this->isHeaderPresent() && $this->inRestrictedGroups($user)) {
			if($this->isLoginPage) {
				$url = $this->urlGenerator->linkToRouteAbsolute('restrictbyheader.LoginDenied.showErrorPage');
				header('Location: ' . $url );
				exit();
			}

			// All other clients
			http_response_code(403);
			exit();
		}
	}

	/**
	 * Registers the handleLoginRequest to attach to the PreLogin hook.  
	 */
	public function register() {
		\OCP\Util::connectHook(
			'OC_User',
			'pre_login',
			$this,
			'handleLoginRequest'
		);
	}
}
