<?php

namespace OCA\RestrictByHeader\Controller;

use OCP\AppFramework\Controller;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\TemplateResponse;

class LoginDeniedController extends Controller {
	/**
	 * @PublicPage
	 * @NoCSRFRequired
	 * @return TemplateResponse
	 */
	public function showErrorPage() {
		$response = new TemplateResponse(
			$this->appName,
			'errorPage',
			[],
			'guest'
		);
		$response->setStatus(Http::STATUS_FORBIDDEN);

		return $response;
	}
}
