<?php

namespace OCA\RestrictByHeader\Settings;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\Settings\ISettings;

class LimitSettings implements ISettings {

	public function getForm() {
		return new TemplateResponse('restrictbyheader', 'admin-settings');
	}

	public function getSection() {
		return 'security';
	}

	public function getPriority() {
		return 49;
	}
}
