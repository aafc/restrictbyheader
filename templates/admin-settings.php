<?php

script('restrictbyheader', ['settings']);
style('restrictbyheader', ['settings']);

?>

<form id="limit-login-by-header" class="section">
	<h2>Prevent login based on HTTP header</h2>
	<em>Restrict specified groups from logging in if "lli-source" is set to "world". "lli-source" is an HTTP header that is set when passing through LLI WAF.</em>

	<br/>
	<table id="limit-login-by-header-list">
	</table>

	<div id="limit-login-by-header-input-fields">
		<select id="limit-login-by-header-grouplist" style="width: 200px;">
		</select>
		<input type="button" id="limit-login-by-header-submit" value="<?php p($l->t('Add')); ?>">
	</div>
</form>
