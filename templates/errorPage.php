<div class="error">
	<h2><?php p($l->t('Not authorized')) ?></h2>
	<p><?php p('Login from your IP is not permitted for this user.'); ?></p>
</div>
