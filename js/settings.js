(function(OCA) {
	OCA.RestrictByHeader = OCA.RestrictByHeader || {};
	OCA.RestrictByHeader.RestrictedGroups = [];
	OCA.RestrictByHeader.AllGroups = [];

	/**
	 * @namespace OCA.RestrictByHeader.Settings
	 */
	OCA.RestrictByHeader.Settings = {
		load: function() {
			OCP.AppConfig.getValue(
				'restrictbyheader',
				'blacklisted.groups',
				'',
				{
					success: function(data) {
						var textData =  $(data).find('data data').text();
						var groups = textData.split(',');
		 				var table = document.getElementById('limit-login-by-header-list');
		 				table.innerHTML = '';

						if (textData === '') { return; }	// no groups added yet
						OCA.RestrictByHeader.RestrictedGroups = groups;

						groups.forEach(function(range) {
							var row = table.insertRow(0);
							var actionCell = row.insertCell(0);
							actionCell.className = 'action-column';
							var ipCell = row.insertCell(0);

							var ipCellValue = document.createElement('span');
							ipCellValue.innerText = range;
							ipCell.innerHTML = ipCellValue.outerHTML;

							var actionCellValue = document.createElement('span');
							var deleteLink = document.createElement('a');
							deleteLink.className = 'icon-delete has-tooltip';
							deleteLink.title = t('restrictbyheader', 'Delete');
							deleteLink.setAttribute('data', range);
							actionCellValue.innerHTML = deleteLink.outerHTML;
							actionCell.innerHTML = actionCellValue.outerHTML;
						});
					}
				}
			);
		},

		storeGroups: function() {
			var groups = OCA.RestrictByHeader.RestrictedGroups.join();
			OCP.AppConfig.setValue(
				'restrictbyheader',
				'blacklisted.groups',
				groups,
				{
					success: function () {
						OCA.RestrictByHeader.Settings.load();
					}
				}
			);
		},

		addGroup: function(group) {
			OCA.RestrictByHeader.RestrictedGroups.push(group);
			OCA.RestrictByHeader.Settings.storeGroups();
		},

		removeGroup: function(group) {
			var index = OCA.RestrictByHeader.RestrictedGroups.indexOf(group);
			OCA.RestrictByHeader.RestrictedGroups.splice(index, 1);
			OCA.RestrictByHeader.Settings.storeGroups();
		},
	};

})(OCA);

(function () {
	OCA.RestrictByHeader.Settings.load();

	$('#limit-login-by-header-submit').click(function() {
        var group = $('#limit-login-by-header-grouplist').val();
		OCA.RestrictByHeader.Settings.addGroup(group);
	});

	$('#limit-login-by-header-list').on('click', 'a', function() {
		var group = $(this).attr('data');
		OCA.RestrictByHeader.Settings.removeGroup(group);
	});

	// Make ajax request to get group listing. Populate drop-down menu
	$.ajax({
		url: OC.linkToOCS('cloud', 2) + '/groups',
		dataType: 'json',
		success: function(response) {
			const groups = response.ocs.data.groups;
			$.each(groups, function (i, item) {
				$('#limit-login-by-header-grouplist').append($('<option>', { 
					value: item,
					text : item
				}));
			});
		}
	});
})();