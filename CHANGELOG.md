# Changelog

All notable changes to this project will be added in this file. Changes are released together in a specified build. Tags (for the builds) are created after a new entry in this file is added.

## 1.00.00 - 2020-02-24

### Added

- Ability to alter groups in settings for sysadmins
- Basic functionality added
