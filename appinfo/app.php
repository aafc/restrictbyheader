<?php

// Parameters to build LoginHookListener
$config = \OC::$server->getConfig();
$request = \OC::$server->getRequest();
$urlGenerator = \OC::$server->getURLGenerator();
$groupManager = \OC::$server->getGroupManager();
$isLoginpage = false;

// Check if user is using Web UI. Will display different error message if they are
if($request->getRequestUri() === $urlGenerator->linkToRoute('core.login.showLoginForm')) {
	$isLoginpage = true;
}

$loginHookListener = new \OCA\RestrictByHeader\LoginHookListener(
	$config,
	$request,
	$groupManager, 
	$urlGenerator,
	$isLoginpage
);

$loginHookListener->register();
