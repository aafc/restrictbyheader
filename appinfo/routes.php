<?php

return [
	'routes' => [
		[
			'name' => 'LoginDenied#showErrorPage',
			'url' => '/denied',
			'verb' => 'GET'
		],
	],
];